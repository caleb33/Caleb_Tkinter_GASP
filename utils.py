import string


def read_number(prompt="Please enter a number: "):
    if prompt != "" and prompt[-1] not in string.whitespace:
        prompt = prompt + " "
    while True:
        result = input(prompt)
        try:
            num = int(result)
            return num
        except ValueError:
            try:
                num = float(result)
                return num
            except ValueError:
                print("But that wasn't a number!")


def read_yesorno(prompt="Yes or no? "):
    if prompt != "" and prompt[-1] not in string.whitespace:
        prompt = prompt + " "
    while True:
        result = input(prompt)
        try:
            result = string.lower(string.split(result)[0])
        except:
            result = ""
        if result == "yes" or result == "y":
            return True
        if result == "no" or result == "n":
            return False
        print("Please answer yes or no.")
